import React from "react";
import {Link} from "react-router-dom";
import useForm from "../../hooks/useForm";
import axios from "axios";
import {API_URL} from "../../constants";
import {useHistory} from "react-router-dom";

// https://remotestack.io/react-bootstrap-login-register-ui-templates/
const SignUp = () => {
    const [userName, setUserName, resetUserName] = useForm('')
    const [password1, setPassword1, resetPassword1] = useForm('')
    const [password2, setPassword2, resetPassword2] = useForm('')
    const history = useHistory()

    const registerUser = (event) => {
        event.preventDefault()
        axios.post(`${API_URL}/auth/registration/`, {
            username: userName,
            password1: password1,
            password2: password2,
        }).then(res => {
            console.log('res.data register: ', res.data)
            history.push('/sign-in')
        }).catch(err => {
            console.log('err: ', err)
        })
        resetUserName()
        resetPassword1()
        resetPassword2()
    }

    return (
        <form onSubmit={registerUser}>
            <h3 className="text-size-big">ثبت نام</h3>

            <div className="form-group form-group_spec">
                <label className="text-size-medium" style={{textAlign: 'right'}}>نام کاربری</label>
                <input
                    type="text"
                    style={{textAlign:'right'}}
                    value={userName}
                    onChange={setUserName}
                    className="form-control text-size-small"
                    placeholder="نام کاربری را وارد کنید"/>
            </div>

            <div className="form-group form-group_spec">
                <label className="text-size-medium" style={{textAlign: 'right'}}>رمز</label>
                <input
                    type="password"
                    style={{textAlign:'right'}}
                    value={password1}
                    onChange={setPassword1}
                    className="form-control text-size-small"
                    placeholder="رمز را وارد کنید"/>
            </div>

            <div className="form-group form-group_spec">
                <label className="text-size-medium" style={{textAlign: 'right'}}>تکرار رمز</label>
                <input
                    type="password"
                    style={{textAlign:'right'}}
                    value={password2}
                    onChange={setPassword2}
                    className="form-control text-size-small"
                    placeholder="رمز را دوباره وارد کنید"/>
            </div>


            <button type="submit" style={{float: 'right'}}
                    className="btn btn-dark btn-lg btn-block text-size-small">
                ثبت نام
            </button>
            <div className="clearfix"/>

            <p className="forgot-password text-right text-size-medium">
                قبلا ثبت نام کرده اید؟ <Link className="text-size-medium text-black" to={"/sign-in"}>ورود</Link>
            </p>

        </form>
    )
}

export default SignUp