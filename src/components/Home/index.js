import React, {useEffect, useState} from 'react';
import Tree from "./Tree";
import axios from "axios";
import {API_URL} from "../../constants";
import useForm from "../../hooks/useForm";
import {Select} from 'semantic-ui-react'
// import ModalForm from "./Modal";
import BasicModal from "./Modal";
import useFormTwo from "../../hooks/useFormTwo";

const RowNumbers = [
    {key: 0, value: 0, text: 'سطر ۰'},
    {key: 1, value: 1, text: 'سطر ۱'},
    {key: 2, value: 2, text: 'سطر ۲'},
    {key: 3, value: 3, text: 'سطر ۳'},
    {key: 4, value: 4, text: 'سطر ۴'},
    {key: 5, value: 5, text: 'سطر ۵'},
    {key: 6, value: 6, text: 'سطر ۶'},
    {key: 7, value: 7, text: 'سطر ۷'},
    {key: 8, value: 8, text: 'سطر ۸'},
    {key: 9, value: 9, text: 'سطر ۹'},
    {key: 10, value: 10, text: 'سطر ۱۰'}
]

function Home(props) {
    const [treeNameInput, setTreeNameInput, resetTreeNameInput] = useForm('')
    const [treeRowNumber, setTreeRowNumber, resetTreeRowNumber] = useForm('')
    // const [treeLoad, setTreeLoad, resetTreeLoad] = useForm(0)
    // const [trees, setTrees] = useState([])

    const [nodeNameInput, setNodeNameInput, resetNodeNameInput] = useForm('')
    const [refNodeInput, setRefNodeInput, resetRefNodeInput] = useForm(0)
    const [rowNumInput, setRowNumInput, resetRowNumInput] = useForm(0)
    const [positionInput, setPositionInput] = useState(150)

    const [refNodes, setRefNodes] = useState([{key: -100, value: 'Null', text: 'Null'},])
    const [nodeNumbers, setNodeNumbers] = useState(0)
    const [nodeUnit, setNodeUnit] = useState(150)
    const [propertiesChanged, setPropertiesChanged] = useState(0)

    const treeId = parseInt(localStorage.getItem('treeId'))

    const POSITION_X = [
        {key: 1, value: nodeUnit, text: 'وسط'},

        {key: 2, value: nodeUnit + (2 * 100), text: 'راست ۱'},
        {key: 3, value: nodeUnit + (4 * 100), text: 'راست ۲'},
        {key: 4, value: nodeUnit + (6 * 100), text: 'راست ۳'},
        {key: 5, value: nodeUnit + (8 * 100), text: 'راست ۴'},
        {key: 6, value: nodeUnit + (10 * 100), text: 'راست ۵'},
        {key: 7, value: nodeUnit + (12 * 100), text: 'راست ۶'},
        {key: 8, value: nodeUnit + (14 * 100), text: 'راست ۷'},
        {key: 9, value: nodeUnit + (16 * 100), text: 'راست ۸'},
        {key: 10, value: nodeUnit + (18 * 100), text: 'راست ۹'},
        {key: 11, value: nodeUnit + (20 * 100), text: 'راست ۱۰'},

        {key: 12, value: nodeUnit - (2 * 100), text: 'چپ ۱'},
        {key: 13, value: nodeUnit - (4 * 100), text: 'چپ ۲'},
        {key: 14, value: nodeUnit - (6 * 100), text: 'چپ ۳'},
        {key: 15, value: nodeUnit - (8 * 100), text: 'چپ ۴'},
        {key: 16, value: nodeUnit - (10 * 100), text: 'چپ ۵'},
        {key: 17, value: nodeUnit - (12 * 100), text: 'چپ ۶'},
        {key: 18, value: nodeUnit - (14 * 100), text: 'چپ ۷'},
        {key: 19, value: nodeUnit - (16 * 100), text: 'چپ ۸'},
        {key: 20, value: nodeUnit - (18 * 100), text: 'چپ ۹'},
        {key: 21, value: nodeUnit - (20 * 100), text: 'چپ ۱۰'}
    ]

    // console.log('HOME')
    // console.log('nodeUnit: ', nodeUnit)
    // console.log('positionInput: ', positionInput)
    // console.log('nodeNumbers: ', nodeNumbers)
    // console.log('propertiesChanged: ', propertiesChanged)

    useEffect(() => {
        const getRefNode = async () => {
            const response = await axios.get(`${API_URL}/node/ref/${refNodeInput}/`)
            console.log('response.data.node_horizontal_position: ', response.data.node_horizontal_position)
            setNodeUnit(response.data.node_horizontal_position)
        }
        getRefNode()
    }, [refNodeInput])

    useEffect(() => {
        const getRefNodes = async () => {
            let ref_nodes = []
            const response = await axios.get(`${API_URL}/node/ref/list/${treeId}/`)
            console.log('res.data ref: ', response.data)
            const uniqueResponse = Array.from(new Set(response.data.map(node => node.node_name))).map(node_name => {
                return response.data.find(node => node.node_name === node_name)
            })
            ref_nodes = uniqueResponse.map(node => (
                {
                    key: parseInt(node.id),
                    value: parseInt(node.id),
                    text: node.node_name
                }
            ))
            ref_nodes.push({
                key: -100,
                value: 'Null',
                text: 'Null'
            })
            console.log('ref_nodes: ', ref_nodes)
            setRefNodes(ref_nodes)
        }
        getRefNodes()
    }, [nodeNumbers])

    // useEffect(() => {
    //     const getTrees = async () => {
    //         let tree_list = []
    //         const response = await axios.get(`${API_URL}/tree/all/`)
    //         let trees = response.data
    //         console.log('trees: ', trees)
    //         trees.map(tree => (
    //             tree_list.push(
    //                 {
    //                     key: tree.id,
    //                     value: tree.id,
    //                     text: tree.tree_name
    //                 }
    //             )
    //         ))
    //         setTrees(tree_list)
    //     }
    //     getTrees()
    // }, [])

    // useEffect(() => {
    //     const loadTree = async () => {
    //         localStorage.setItem('treeId', treeLoad.toString())
    //     }
    //     loadTree()
    // }, [treeLoad])

    const createTree = (event) => {
        event.preventDefault()
        axios.post(`${API_URL}/tree/create/`, {
            tree_name: treeNameInput,
            row_numbers: parseInt(treeRowNumber),
        }).then(res => {
            console.log('res.data tree: ', res.data)
            localStorage.setItem('treeId', res.data.id)
        }).catch(err => {
            console.log('err tree: ', err)
        })
        resetTreeNameInput()
        resetTreeRowNumber()
    }

    const createNode = async (event) => {
        event.preventDefault()
        // console.log('refNodeInput:', refNodeInput)
        // console.log('rowNumInput:', rowNumInput)
        await axios.post(`${API_URL}/node/create/`, {
            tree: parseInt(treeId),
            node_name: nodeNameInput,
            ref_node: parseInt(refNodeInput),
            row_name_number: rowNumInput,
            node_horizontal_position: positionInput
        })

        // const nodes = await getRefNodes()
        // // console.log('nodes: ', nodes)
        // setRefNodes(nodes)
        setNodeNumbers((prevState) => prevState + 1)
        resetNodeNameInput()
        resetRefNodeInput()
        resetRowNumInput()
        setPositionInput(nodeUnit)
    }

    const handlePositionChange = (event, data) => {
        let targetData
        event.target.value ? targetData = event.target.value : targetData = data.value
        console.log('targetData: ', targetData)
        setPositionInput(targetData)
    }

    return (
        <div>
            <div className="forms">
                <form className="tree__form" onSubmit={createTree}>
                    <div className="form-group tree__group">
                        <input
                            type="text"
                            value={treeNameInput}
                            onChange={setTreeNameInput}
                            className="form-control"
                            style={{textAlign: 'right'}}
                            placeholder="نام درخت"/>
                        <input
                            type="text"
                            value={treeRowNumber}
                            onChange={setTreeRowNumber}
                            className="form-control"
                            style={{textAlign: 'right'}}
                            placeholder="تعداد سطرهای درخت"/>
                    </div>
                    {/*<div className="form-group tree__group">*/}
                    {/*    */}
                    {/*</div>*/}
                    <div className="form-group tree__group">
                        <button
                            type="submit"
                            className="btn btn-dark btn-lg">
                            ساخت درخت
                        </button>
                        {/*<Select*/}
                        {/*    value={treeLoad}*/}
                        {/*    onChange={setTreeLoad}*/}
                        {/*    style={{textAlign: 'right'}}*/}
                        {/*    className="form-control"*/}
                        {/*    placeholder='درخت را انتخاب کنید'*/}
                        {/*    options={trees}/>*/}
                    </div>
                </form>

                <form className="node__form" onSubmit={createNode}>
                    <div className="form-group node__group">
                        <input
                            type="text"
                            value={nodeNameInput}
                            onChange={setNodeNameInput}
                            className="form-control"
                            style={{textAlign: 'right'}}
                            placeholder="نام گره را وارد کنید"/>
                        <Select
                            value={positionInput}
                            onChange={handlePositionChange}
                            className="form-control"
                            style={{textAlign: 'right'}}
                            placeholder='Enter Horizontal Position...'
                            options={POSITION_X}/>
                        <button type="submit"
                                className="btn btn-dark btn-lg">
                            ساخت گره
                        </button>
                    </div>
                    <div className="form-group node__group">
                        <Select
                            value={refNodeInput}
                            onChange={setRefNodeInput}
                            className="form-control"
                            style={{textAlign: 'right'}}
                            placeholder='گره والد را انتخاب کنید'
                            options={refNodes}/>
                        <Select
                            value={rowNumInput}
                            onChange={setRowNumInput}
                            className="form-control"
                            style={{textAlign: 'right'}}
                            placeholder='شماره سطر گره را وارد کنید'
                            options={RowNumbers}/>
                    </div>
                </form>

                <div className="form-group node__property">
                    <BasicModal
                        treeId={treeId}
                        Nodes={nodeNumbers}
                        setPropertiesChanged={setPropertiesChanged}
                    />
                </div>

            </div>
            <Tree
                treeId={treeId}
                Nodes={nodeNumbers}
                propertiesChanged={propertiesChanged}
                setPropertiesChanged={setPropertiesChanged}
            />
        </div>
    );
}

export default Home;