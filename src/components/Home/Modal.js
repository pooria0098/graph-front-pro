import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import {useEffect, useState} from "react";
import {Select} from 'semantic-ui-react'
import axios from "axios";
import {API_URL} from "../../constants";
import useForm from "../../hooks/useForm";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function BasicModal({treeId, Nodes, setPropertiesChanged}) {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const [nodeInput, setNodeInput, resetNodeInput] = useForm(0)
    const [propertyName, setPropertyName] = useState('')
    const [nodes, setNodes] = useState([])
    const [properties, setProperties] = useState([])

    useEffect(() => {
        const getNodes = async () => {
            let nodeList = []
            const response = await axios.get(`${API_URL}/node/list/${treeId}/`)
            console.log('res.data modal: ', response.data)
            let uniqueResponse;
            uniqueResponse = Array.from(new Set(response.data.reverse().map(node => node.node_name))).map(node_name => {
                return response.data.find(node => node.node_name === node_name)
            });
            nodeList = uniqueResponse.map(node => (
                {
                    key: parseInt(node.id),
                    value: parseInt(node.id),
                    text: node.node_name
                }
            ))
            console.log('nodes: ', nodeList)
            setNodes(nodeList)
        }
        getNodes()
    }, [Nodes])

    const handleChangeShareholder = idx => evt => {
        const newProperties = properties.map((property, sidx) => {
            if (idx !== sidx) return property;
            console.log('evt.target.value: ', evt.target.value)
            return {node: nodeInput, property_name: evt.target.value};
        })
        setProperties(newProperties)
    };

    const handleSubmit = evt => {
        evt.preventDefault()
        console.log('properties: ', properties)
        // const dummy_data = [
        //     {
        //         "node": 92,
        //         "property_name": "string1"
        //     },
        //     {
        //         "node": 92,
        //         "property_name": "string2"
        //     },
        //     {
        //         "node": 92,
        //         "property_name": "string3"
        //     }
        // ]
        // const json_response = JSON.stringify(properties)
        axios.post(`${API_URL}/node-property/`, properties
        ).then(res => {
            console.log('res.data modal: ', res.data)
        }).catch(err => {
            console.log('err modal: ', err)
        })
        resetNodeInput()
        setPropertyName('')
        setProperties([])
        handleClose()
        setPropertiesChanged((prevState) => prevState + 1)
    };

    const handleAddShareholder = () => {
        setProperties(properties.concat(
            [{
                node: nodeInput,
                property_name: propertyName,
            }]
        ))
    }

    const handleRemoveShareholder = idx => () => {
        setProperties(properties.filter((prop, sidx) => idx !== sidx))
    };

    return (
        <div>
            <Button id='btn' onClick={handleOpen}>
                افزودن ویژگی
            </Button>
            <Button id='btn' onClick={handleOpen}>
                ویرایش ویژگی
            </Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description">
                <Box sx={style}>
                    {/**/}
                    <form onSubmit={handleSubmit} className="modall__form">
                        <Select
                            value={nodeInput}
                            onChange={setNodeInput}
                            className="form-control"
                            placeholder='گره مورد نظر را انتخاب کنید'
                            options={nodes}/>

                        {properties.map((obj, idx) => (
                            <div className="shareholder" key={idx}>
                                <input
                                    value={properties.property_name}
                                    onChange={handleChangeShareholder(idx)}
                                    type="text"
                                    placeholder={`Property #${idx + 1}`}/>
                                <button
                                    type="button"
                                    onClick={handleRemoveShareholder(idx)}
                                    className="small"> X
                                </button>
                            </div>
                        ))}
                        <button
                            type="button"
                            onClick={handleAddShareholder}
                            className="small">
                            افزودن ویژگی
                        </button>
                        <button>ارسال</button>
                    </form>
                    {/**/}
                </Box>
            </Modal>
        </div>
    );
}