import React, {useState} from 'react';
import Modal from "./Modal";

// import ReactHTMLTableToExcel from "react-html-table-to-excel";

/*
https://stackoverflow.com/questions/53888948/react-table-individual-cell-style
https://retool.com/blog/building-a-react-table-component/
https://github.com/tannerlinsley/react-table
https://blog.logrocket.com/complete-guide-building-smart-data-table-react/
https://github.com/adazzle/react-data-grid
https://adazzle.github.io/react-data-grid/#/common-features
https://www.pluralsight.com/guides/dynamic-tables-from-editable-columns-in-react-html
https://github.com/SheetJS/sheetjs/tree/ebfb5bc7c901ed2d3a492065f9d11071282185e6/demos/react

https://codesandbox.io/examples/package/react-html-table-to-excel
https://codesandbox.io/s/14uvl?file=/src/index.js:210-441
https://codesandbox.io/s/rx8z6
https://stackoverflow.com/questions/61316889/how-to-export-data-to-excel-using-react-libraries
*/
function Chart({item, updateItemList}) {
    // console.log('item: ', item)
    const {index, key, level, nodes, cells} = item
    const caption = `Row_${level}_Node_${key}`
    let new_nodes = [caption]
    let cell_counter = 0

    new_nodes = new_nodes.concat(nodes)
    // console.log('index: ', index)
    // console.log('key: ', key)
    // console.log('level: ', level)
    // console.log('nodes: ', nodes)
    // console.log('new_nodes: ', new_nodes)
    // console.log('cells: ', cells)
    // console.log('-----------------------------------')

    // const [data, setData] = useState([]);
    // const [inEditMode, setInEditMode] = useState({
    //     status: false,
    //     cellKey: null
    // });

    // const onEdit = (id) => {
    //     setInEditMode({
    //         status: true,
    //         cellKey: id
    //     })
    // }

    // const openModal = () => {
    //     console.log('dlfsaklfhdfah')
    //     return (
    //         <Modal/>
    //     )
    // }

    const createTable = () => {
        return (
            <>
                {/*<ReactHTMLTableToExcel*/}
                {/*    id="test-table-xls-button"*/}
                {/*    className="download-table-xls-button"*/}
                {/*    table="tbl_exporttable_to_xls"*/}
                {/*    filename="tablexls"*/}
                {/*    sheet="tablexls"*/}
                {/*    buttonText="Download as XLS"*/}
                {/*/>*/}
                <table id="tbl_exporttable_to_xls">
                    {/*<caption>Row_{level}_Node_{key}</caption>*/}
                    <tr>
                        {new_nodes.map((node, index) => (
                            index === 0
                                ? <th key={index} style={{padding: 0}}>{node}</th>
                                : <th key={index}>{node}</th>
                        ))}
                    </tr>
                    {createCells_2()}
                </table>
            </>
        )
    }
    // console.log(item)

    const createCells_2 = () => {
        // console.log('CELLS: ', cells)
        return (
            cells.map((row_cells, index_row) => (
                <tr key={index_row}>
                    <th>{nodes[index_row]}</th>
                    {row_cells.map((cell, index_cell) => (
                        <td key={index_cell}
                            // {...cell.data.split(' ').length < 3 && {onClick: openModal}}
                            // onClick={cell.data.split(' ').length < 3 && (() => handleClick(cell.idx))}
                            className={cell.data.trim().split(' ').length < 3 ? 'uncompleted' : ''}>
                            {
                                cell.data.trim().split(' ').length < 3
                                    ? <Modal updateItemList={updateItemList} cell={cell} index={index}/>
                                    : `${cell.data}`
                            }
                            {/*{`${cell.data}(${cell.idx})`}*/}
                            {/*<Modal cell={cell}/>*/}
                        </td>
                    ))}
                </tr>
            ))
        )
    }
    // const createCells = () => {
    //     console.log('cell_counter: ', cell_counter)
    //     return (
    //         nodes.map((node_row, index_row) => (
    //             <tr key={index_row}>
    //                 {/*{new_nodes.map((node_col, index_col) => (*/}
    //                 {/*    index_col === 0*/}
    //                 {/*        ? <th>{node_row}</th>*/}
    //                 {/*        : <td  style={{*/}
    //                 {/*            backgroundColor: cell_counter - index_col < 0 && '#dddddd',*/}
    //                 {/*            border: cell_counter - index_col < 0 && '1px solid #fff',*/}
    //                 {/*            color: cell_counter - index_col === 0 || cell_counter - index_col > 0 ? '#aaa' : '#222'*/}
    //                 {/*        }}>*/}
    //                 {/*            {cell_counter - index_col === 0*/}
    //                 {/*                ? '1, 1, 1'*/}
    //                 {/*                : cell_counter - index_col > 0*/}
    //                 {/*                    ? 'Auto'*/}
    //                 {/*                    : inEditMode*/}
    //                 {/*                        ? <button onClick={ToggleEditMode}/>*/}
    //                 {/*                        : ''*/}
    //                 {/*            }*/}
    //                 {/*        </td>*/}
    //                 {/*), cell_counter++)}*/}
    //
    //                 {/*{new_nodes.map((node_col, index_col) => (*/}
    //                 {/*    index_col === 0*/}
    //                 {/*        ? <th key={index_col}>{node_row}</th>*/}
    //                 {/*        : cell_counter - index_col === 0*/}
    //                 {/*            ? <td key={index_col} style={{color: '#aaa'}}>1, 1, 1</td>*/}
    //                 {/*            : cell_counter - index_col > 0*/}
    //                 {/*                ? <td key={index_col} style={{color: '#aaa'}}>Auto</td>*/}
    //                 {/*                : <td key={index_col} onClick={() => onEdit(cell_counter)}*/}
    //                 {/*                      style={{color: '#aaa', border: '1px solid #fff', backgroundColor: '#dddddd'}}>*/}
    //                 {/*                    {inEditMode.status && inEditMode.cellKey === cell_counter*/}
    //                 {/*                        ? <input type="text"/>*/}
    //                 {/*                        : `${inEditMode.status} ${inEditMode.cellKey} ${index_col} ${cell_counter}`*/}
    //                 {/*                    }*/}
    //                 {/*                </td>*/}
    //                 {/*), cell_counter++)}*/}
    //             </tr>
    //         ))
    //     )
    // }

    return (
        <div style={{marginTop: '3rem'}}>
            {createTable()}
        </div>
    );
}

export default Chart;

