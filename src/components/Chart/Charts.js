import React, {useEffect, useState} from 'react';
import axios from "axios";
import {API_URL} from "../../constants";
import Chart from "./Chart";
import {useHistory, Link} from "react-router-dom";

function Charts(props) {
    const [comingData, setComingData] = useState([]);
    const items_list = []
    const [itemsList, setItemsList] = useState(items_list)
    const [conterRendering, setCounterRendering] = useState(0)
    let history = useHistory();

    useEffect(() => {
        axios.post(`${API_URL}/chart/fill/`, {
            tree_id: parseInt(localStorage.getItem('treeId'))
        }).then(res => {
            console.log('res.data: ', res.data)
            console.log('res.data.length: ', res.data.length)
            setComingData(res.data)
        }).catch(err => {
            console.log('err: ', err)
        })
    }, []);

    useEffect(() => {
        doCalculationOnComingData()
    }, [comingData])

    const updateItemList = async (index, idx, data) => {
        // await setItemsList(() => {
        let items = itemsList
        let new_item_list = items.map(item => item.cells.map(item1 => item1))
        for (let i = 0; i < new_item_list.length; i++) {
            for (let j = 0; j < new_item_list[i].length; j++) {
                let target_obj = new_item_list[i][j].find(o => o.idx === idx)
                if (target_obj) {
                    console.log('target_obj.data b: ', target_obj.data)
                    target_obj.data = target_obj.data + data
                    console.log('target_obj.data f: ', target_obj.data)
                }
            }
        }
        // }, console.log('iiiiitemsList: ', itemsList))

        console.log('itemsssss: ', items)

        // items.map(item => item.index !== 20?item:item.cells.map(it=>it))

        // let new_item_list = itemsList.map(item => item.cells.map(item1 => item1))
        // for (let i = 0; i < new_item_list.length; i++) {
        //     for (let j = 0; j < new_item_list[i].length; j++) {
        //         let target_obj = new_item_list[i][j].find(o => o.idx === idx)
        //         if (target_obj) {
        //             console.log('target_obj b: ', target_obj)
        //             target_obj.data = target_obj.data + data
        //             console.log('target_obj f: ', target_obj)
        //         }
        //     }
        // }

        // console.log('new_item_list: ', new_item_list)
        setItemsList(items)
        setCounterRendering(prevState => prevState + 1)
        // console.log('idx: ', idx)
        // console.log('data: ', data)
    }

    const doCalculationOnComingData = () => {
        // let itemsNumber = 0
        let items_list_index = 1
        let item_index = 0

        console.log('comingData: ', comingData)
        for (let items of comingData) {
            for (let key in items) {
                // console.log('key: ', key)
                // skip loop if the property is from prototype
                if (!items.hasOwnProperty(key)) continue;

                const obj = items[key];
                // console.log('obj: ', obj)

                // create 2d array
                const LENGTH = obj.counts
                let two_d_array = Array.from(Array(LENGTH), () => new Array(LENGTH).fill().map((e, i) => {
                    return {idx: ++item_index}
                }));
                // const LENGTH = obj.counts * obj.counts
                // let two_d_array = new Array(LENGTH).fill().map((e, i) => {
                //     return {idx: ++item_index}
                // })

                // console.log('two_d_array: ', two_d_array)
                for (let i = 0; i < obj.counts; i++) {
                    // console.log('two_d_array[i]: ', two_d_array[i])
                    for (let j = 0; j < obj.counts; j++) {
                        if (i > j) {
                            two_d_array[i][j]['data'] = 'auto auto auto'
                        } else if (i === j) {
                            two_d_array[i][j]['data'] = '1 1 1'
                        } else {
                            two_d_array[i][j]['data'] = ''
                        }
                        // console.log('two_d_array[i][j]: ', two_d_array[i][j])
                    }
                }

                items_list.push({
                    'index': items_list_index,
                    'key': key,
                    'level': obj.level,
                    'nodes': obj.nodes,
                    'cells': two_d_array
                })
                items_list_index++
                // for (const prop in obj) {
                //     // skip loop if the property is from prototype
                //     if (!obj.hasOwnProperty(prop)) continue;
                //
                //     // your code
                //     console.log(prop + " = " + obj[prop]);
                // }
                // itemsNumber += 1
            }
        }
        console.log('items_list: ', items_list)
        if (items_list.length > 0) {
            setItemsList(items_list)
        }
    }

    const renderCharts = () => {
        console.log('renderCharts')
        // doCalculationOnComingData()
        console.log('itemsList: ', itemsList)
        return itemsList.map((item, index) => <Chart
                key={index}
                item={item}
                updateItemList={updateItemList}
                // keys={item.key}
                // counts={item.counts}
                // level={item.level}
                // nodes={item.nodes}
            />
        )
    }

    const checkTableValidation = () => {
        let items = itemsList.map(item => item.cells.map(item1 => item1))
        for (let i = 0; i < items.length; i++) {
            for (let j = 0; j < items[i].length; j++) {
                for (let obj of items[i][j]) {
                    if (obj.data.trim().split(' ').length !== 3) {
                        return false
                    }
                }
            }
        }
        return true
    }

    const submitTables = (event) => {
        event.preventDefault()
        if (checkTableValidation()) {
            axios.post(`${API_URL}/chart/create/web/`, {
                chart: itemsList,
            }).then(res => {
                console.log('res.data: ', res.data)
                alert('جدول بدرستی پر شده است')
            }).catch(err => {
                console.log('err: ', err)
            })
        } else {
            alert('خانه های جدول را کامل پرنکرده اید')
        }
    }

    return (
        <div style={{marginTop: '3800px'}}>
            {renderCharts()}
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <button
                    style={{
                        display: 'inline',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: '2rem',
                        width: '50%',
                        marginRight:'1rem'
                    }}
                    onClick={submitTables}
                    type="submit"
                    className="btn btn-dark form-group-chart text-size-medium btn-lg">
                    ارسال جداول
                </button>
                <Link
                    style={{
                        display: 'inline',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: '2rem',
                        width: '50%'
                    }}
                    className="btn btn-dark form-group-chart text-size-medium btn-lg"
                    to={'/menu'}>بازگشت به منو </Link>
            </div>
            {/*ارسال جداول*/}
        </div>
    )
}

// export default React.memo(Charts);
export default Charts;