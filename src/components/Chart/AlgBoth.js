import React, {useEffect, useState} from 'react';
import axios from "axios";
import {API_URL} from "../../constants";
import {Select} from "semantic-ui-react";
import useForm from "../../hooks/useForm";

function AlgOne(props) {
    const [sheets, setSheets] = useState([]);
    const [selectedSheetName, setSelectedSheetName, resetSelectedSheetName] = useForm('')

    useEffect(() => {
        axios.get(`${API_URL}/chart/alg_both/`
        ).then(res => {
            setSheets(res.data)
            console.log('res: ', res)
        }).catch(err => {
            console.log('err: ', err)
        })
    }, []);

    const sheetList = () => {
        return sheets && sheets.map((res, index) => {
            return {key: index, value: res.sheet_name, text: res.sheet_name}
        })
    }

    const selectSheet = (sheet_name) => {
        // console.log('rrrr: ', sheets.filter(res => res.sheet_name === sheet_name && res))
        // return sheets.filter(res => res.sheet_name === sheet_name && res)
        return sheets.filter(res => res.sheet_name === sheet_name && res)
    }

    const allTables = (selectedSheetName) => {
        console.log('allTables selectedSheetName: ', selectedSheetName)
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const data_headers = res.data.headers
            const data_values = res.data.values
            const alg_one_headers = res.alg_one.headers
            const alg_one_values = res.alg_one.values
            const alg_two_headers = res.alg_two.headers
            const alg_two_values = res.alg_two.values
            const alg_three_headers = res.alg_three.headers
            const alg_three_values = res.alg_three.values
            const alg_four_headers = res.alg_four.headers
            const alg_four_values = res.alg_four.values

            return (
                <>
                    <table>
                        <tr>
                            {data_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {data_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                    <table>
                        <tr>
                            {alg_one_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_one_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                    <table>
                        <tr>
                            {alg_two_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_two_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                    <table>
                        <tr>
                            {alg_three_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_three_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                    <table>
                        <tr>
                            {alg_four_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_four_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                    <hr/>
                    <hr/>
                </>
            )
        } else {
            console.log('FETCHING...')
        }
    }

    const createDataTable = (selectedSheetName) => {
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const data_headers = res.data.headers
            const data_values = res.data.values
            console.log('data_headers: ', data_headers)
            console.log('data_values: ', data_values)

            return (
                <>
                    <table>
                        <tr>
                            {data_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {data_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                </>
            )
        } else {
            console.log('FETCHING DATA...')
        }
    }

    const algOneTable = (selectedSheetName) => {
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const alg_one_headers = res.alg_one.headers
            const alg_one_values = res.alg_one.values
            // console.log('alg_one_headers: ', alg_one_headers)
            // console.log('alg_one_values: ', alg_one_values)

            return (
                <div>
                    <table>
                        <tr>
                            {alg_one_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_one_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                </div>
            )
        } else {
            console.log('FETCHING ALG ONE...')
        }
    }

    const algTwoTable = (selectedSheetName) => {
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const alg_two_headers = res.alg_two.headers
            const alg_two_values = res.alg_two.values
            console.log('alg_two_headers: ', alg_two_headers)
            console.log('alg_two_values: ', alg_two_values)

            return (
                <div>
                    <table>
                        <tr>
                            {alg_two_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_two_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                </div>
            )
        } else {
            console.log('FETCHING ALG TWO...')
        }
    }

    const algThreeTable = (selectedSheetName) => {
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const alg_three_headers = res.alg_three.headers
            const alg_three_values = res.alg_three.values
            console.log('alg_three_headers: ', alg_three_headers)
            console.log('alg_three_values: ', alg_three_values)

            return (
                <div>
                    <table>
                        <tr>
                            {alg_three_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_three_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                </div>
            )
        } else {
            console.log('FETCHING ALG THREE...')
        }
    }

    const algFourTable = (selectedSheetName) => {
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const alg_four_headers = res.alg_four.headers
            const alg_four_values = res.alg_four.values
            console.log('alg_four_headers: ', alg_four_headers)
            console.log('alg_four_values: ', alg_four_values)

            return (
                <table>
                    <tr>
                        {alg_four_headers.map((header, index) => (
                            <th key={index}>{header}</th>
                        ))}
                    </tr>
                    {alg_four_values.map((value_list, index_row) => (
                        <tr key={index_row}>
                            <th>{value_list[0]}</th>
                            {value_list.map((value, index) => (
                                index > 0 && <td>{value}</td>
                            ))}
                        </tr>
                    ))}
                </table>
            )
        } else {
            console.log('FETCHING ALG THREE...')
        }
    }

    console.log('selectedSheetName: ', selectedSheetName)
    return (
        <div style={{marginTop: '100px'}}>
            {selectedSheetName
                ? <div style={{marginTop: '300px'}}>
                    <Select
                        style={{backgroundColor: '#222', color: '#fff', fontSize: '2rem', marginTop: '2rem'}}
                        value={selectedSheetName}
                        onChange={setSelectedSheetName}
                        className="form-control"
                        placeholder='گره والد را انتخاب کنید'
                        options={sheetList()}/>
                    <hr/>
                    {createDataTable(selectedSheetName)}
                    {algOneTable(selectedSheetName)}
                    {algTwoTable(selectedSheetName)}
                    {algThreeTable(selectedSheetName)}
                    {algFourTable(selectedSheetName)}
                </div>
                : <div style={{marginTop: '20100px'}}>
                    <Select
                        style={{backgroundColor: '#222', color: '#fff', fontSize: '2rem', marginTop: '2rem'}}
                        value={selectedSheetName}
                        onChange={setSelectedSheetName}
                        className="form-control"
                        placeholder='گره والد را انتخاب کنید'
                        options={sheetList()}/>
                    <hr/>
                    {sheets.map(res => allTables(res.sheet_name))}
                </div>
            }
        </div>
    )
}

export default AlgOne;