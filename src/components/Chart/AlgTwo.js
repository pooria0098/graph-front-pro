import React, {useEffect, useState} from 'react';
import axios from "axios";
import {API_URL} from "../../constants";
import {Select} from "semantic-ui-react";
import useForm from "../../hooks/useForm";

function AlgOne(props) {
    const [sheets, setSheets] = useState([]);
    const [selectedSheetName, setSelectedSheetName, resetSelectedSheetName] = useForm('')

    useEffect(() => {
        axios.get(`${API_URL}/chart/alg_two/`
        ).then(res => {
            setSheets(res.data)
        }).catch(err => {
            console.log('err: ', err)
        })
    }, []);

    const sheetList = () => {
        return sheets && sheets.map((res, index) => {
            return {key: index, value: res.sheet_name, text: res.sheet_name}
        })
    }

    const selectSheet = (sheet_name) => {
        // console.log('rrrr: ', sheets.filter(res => res.sheet_name === sheet_name && res))
        // return sheets.filter(res => res.sheet_name === sheet_name && res)
        return sheets.filter(res => res.sheet_name === sheet_name && res)
    }

    const createDataTable = () => {
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const data_headers = res.data.headers
            const data_values = res.data.values
            console.log('data_headers: ', data_headers)
            console.log('data_values: ', data_values)

            return (
                <>
                    <table>
                        <tr>
                            {data_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {data_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                </>
            )
        } else {
            console.log('FETCHING DATA...')
        }
    }

    const algOneTable = () => {
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const alg_one_headers = res.alg_one.headers
            const alg_one_values = res.alg_one.values
            // console.log('alg_one_headers: ', alg_one_headers)
            // console.log('alg_one_values: ', alg_one_values)

            return (
                <div>
                    <table>
                        <tr>
                            {alg_one_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_one_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                </div>
            )
        } else {
            console.log('FETCHING ALG ONE...')
        }
    }

    const allTables = (selectedSheetName) => {
        console.log('allTables selectedSheetName: ', selectedSheetName)
        const res = selectSheet(selectedSheetName)[0]
        if (res) {
            const data_headers = res.data.headers
            const data_values = res.data.values
            const alg_one_headers = res.alg_one.headers
            const alg_one_values = res.alg_one.values

            return (
                <>
                    <table>
                        <tr>
                            {data_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {data_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                    <table>
                        <tr>
                            {alg_one_headers.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                        </tr>
                        {alg_one_values.map((value_list, index_row) => (
                            <tr key={index_row}>
                                <th>{value_list[0]}</th>
                                {value_list.map((value, index) => (
                                    index > 0 && <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <hr/>
                    <hr/>
                    <hr/>
                    <hr/>
                </>
            )
        } else {
            console.log('FETCHING...')
        }
    }

    return (
        <div style={{marginTop: '100px'}}>
            {selectedSheetName
                ? <div style={{marginTop: '300px'}}>
                    <Select
                        style={{backgroundColor: '#222', color: '#fff', fontSize: '2rem', marginTop: '2rem'}}
                        value={selectedSheetName}
                        onChange={setSelectedSheetName}
                        className="form-control"
                        placeholder='گره والد را انتخاب کنید'
                        options={sheetList()}/>
                    <hr/>
                    {createDataTable(selectedSheetName)}
                    {algOneTable(selectedSheetName)}
                </div>
                : <div style={{marginTop: '6500px'}}>
                    <Select
                        style={{backgroundColor: '#222', color: '#fff', fontSize: '2rem', marginTop: '2rem'}}
                        value={selectedSheetName}
                        onChange={setSelectedSheetName}
                        className="form-control"
                        placeholder='گره والد را انتخاب کنید'
                        options={sheetList()}/>
                    <hr/>
                    {sheets.map(res => allTables(res.sheet_name))}
                </div>
            }
        </div>
    )
}

export default AlgOne;