import React, {useState} from 'react'
import {Button, Modal} from 'semantic-ui-react'
import useForm from "../../hooks/useForm";
import useTrimForm from "../../hooks/useTrimForm";

function exampleReducer(state, action) {
    switch (action.type) {
        case 'close':
            return {open: false}
        case 'open':
            return {open: true, size: action.size}
        default:
            throw new Error('Unsupported action...')
    }
}

const ModalExampleSize = ({cell, updateItemList, index}) => {
    const [state, dispatch] = React.useReducer(exampleReducer, {
        open: false,
        size: undefined,
    })
    const {open, size} = state
    let [data, setData, resetData] = useTrimForm('')
    let [data1, setData1, resetData1] = useTrimForm('')
    let [data2, setData2, resetData2] = useTrimForm('')
    let [data3, setData3, resetData3] = useTrimForm('')
    const [activeInputStatus, setActiveInputStatus] = useState('')

    const makeMultipleInputActive = () => (
        setActiveInputStatus('multiple')
    )

    const makeSingleInputActive = () => {
        setActiveInputStatus('single')
    }

    const submitCellData = (event) => {
        // console.log('eval(data).toFixed(2): ',eval(data).toFixed(2))
        event.preventDefault()
        let new_cell = ''
        if (data) {
            data = eval(data).toFixed(2)
            new_cell = `${data} `
        } else {
            let num_count = 0
            if (data1) {
                data1 = eval(data1).toFixed(2)
                num_count += 1
            }
            if (data2) {
                data2 = eval(data2).toFixed(2)
                num_count += 1
            }
            if (data3) {
                data3 = eval(data3).toFixed(2)
                num_count += 1
            }
            if (num_count < 2) {
                alert('حداقل دو عدد وارد کنید')
                return
            }
            new_cell = `${data1} ${data2} ${data3}`
        }

        updateItemList(index, cell.idx, new_cell)
        resetData()
        resetData1()
        resetData2()
        resetData3()
        setActiveInputStatus('')
        dispatch({type: 'close'})
    }

    // const handleSubmitClick = () => {
    //     dispatch({type: 'close'})
    //     setActiveInputStatus('')
    // }

    return (
        <>
            <Button style={{background: 'none', fontSize: '16px'}}
                    onClick={() => dispatch({type: 'open', size: 'mini'})}>
                {`${cell.data}`}
            </Button>

            <Modal
                size={size}
                open={open}
                onClose={() => dispatch({type: 'close'})}
            >
                <Modal.Header>افزودن داده</Modal.Header>
                <Modal.Content>
                    <p>نوع افزودن داده را انتخاب کنید</p>
                </Modal.Content>

                <Modal.Actions>
                    <Button disabled={cell.data.trim().split(' ')[0] !== ''} negative onClick={makeMultipleInputActive}>
                        افزودن چندتایی داده
                    </Button>
                    <Button positive onClick={makeSingleInputActive}>
                        افزودن تکی داده
                    </Button>
                </Modal.Actions>

                <form onSubmit={submitCellData}>
                    {activeInputStatus === 'multiple'
                        ? <div className="form-group" style={{display: 'flex'}}>
                            <input
                                style={{width: '100px'}}
                                type="text"
                                value={data1}
                                onChange={setData1}
                                className="form-control text-size-small"
                                placeholder="ورود عدد"/>
                            <input
                                style={{width: '100px'}}
                                type="text"
                                value={data2}
                                onChange={setData2}
                                className="form-control text-size-small"
                                placeholder="ورود عدد"/>
                            <input
                                style={{width: '100px'}}
                                type="text"
                                value={data3}
                                onChange={setData3}
                                className="form-control text-size-small"
                                placeholder="ورود عدد"/>
                        </div>
                        : activeInputStatus === 'single'
                            ? <div className="form-group">
                                <input
                                    style={{width: '100px'}}
                                    type="text"
                                    value={data}
                                    onChange={setData}
                                    className="form-control text-size-small"
                                    placeholder="ورود عدد"/>
                            </div>
                            : ''
                    }

                    {activeInputStatus !== '' &&
                    <button type="submit"
                        // onClick={handleSubmitClick}
                            className="btn btn-dark btn-lg btn-block text-size-small">
                        ارسال
                    </button>}


                </form>
            </Modal>
        </>
    )
}

export default ModalExampleSize;


