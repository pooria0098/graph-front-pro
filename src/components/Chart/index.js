import React, {useState} from 'react';
import axios from "axios";
import {API_URL} from "../../constants";
import {Link} from "react-router-dom";
import {Select} from 'semantic-ui-react'

function Chart(props) {
    const [selectedFile, setSelectedFile] = useState(null)

    const createExcels = (event) => {
        event.preventDefault()
        axios.post(`${API_URL}/chart/create/`, {
            tree_id: parseInt(localStorage.getItem('treeId')),
        }).then(res => {
            console.log('res.data: ', res.data)
            console.log('res.data.excel_path: ', res.data.excel_path)
            window.open(res.data.excel_path)
        }).catch(err => {
            console.log('err: ', err)
        })
    }

    const doMethFunction = (event) => {
        event.preventDefault()
        axios.post(`${API_URL}/chart/mesh/`, {
            tree_id: parseInt(localStorage.getItem('treeId')),
        }).then(res => {
            console.log('res.data: ', res.data)
            alert('الگوریتم اول با موفقیت اعمال شد')
        }).catch(err => {
            console.log('err: ', err)
        })
        // return <Link to={'/alg-one'}/>
    }

    const doMethFunction_2 = (event) => {
        event.preventDefault()
        axios.post(`${API_URL}/chart/mesh_2/`, {
            tree_id: parseInt(localStorage.getItem('treeId')),
        }).then(res => {
            console.log('res.data: ', res.data)
            alert('الگوریتم دوم با موفقیت اعمال شد')
        }).catch(err => {
            console.log('err: ', err)
        })
        // return <Link to={'/alg-one'}/>
    }

    const onFileUpload = (event) => {
        event.preventDefault()

        // Create an object of formData
        const formData = new FormData();

        // Update the formData object
        formData.append(
            "excel_file",
            selectedFile,
            // selectedFile.name
        );

        // Details of the uploaded file
        console.log('selectedFile: ', selectedFile);
        console.log('formData: ', formData);

        axios.post(`${API_URL}/chart/send_excel/`,
            formData
        )
    }

    const onFileChange = (event) => {
        setSelectedFile(event.target.files[0])
        onFileUpload(event)
    }

    return (
        <div>
            <div className="form-group tree__group chart_form">
                <button
                    onClick={createExcels}
                    type="submit"
                    className="btn btn-dark form-group-chart btn-lg text-size-medium">
                    پرکردن جداول از طریق اکسل
                </button>
                <button
                    type="submit"
                    className="btn btn-dark form-group-chart text-size-medium btn-lg">
                    <Link to={'/charts'}>پرکردن جداول از طریق وب</Link>
                </button>
            </div>
            <div className="form-group tree__group chart_form">
                <button
                    onClick={doMethFunction}
                    type="submit"
                    className="btn btn-dark form-group-chart text-size-medium btn-lg text-size">
                    اجرای الگوریتم اول
                    {/*<Link to={'/alg-one'}>اجرای الگوریتم اول </Link>*/}
                </button>
                <div
                    className="btn btn-dark form-group-chart text-size-medium btn-lg text-size p-left-medium p-right-medium">
                    <input type="file"
                           onChange={onFileChange}
                           accept="application/vnd.sealed.xls , .xlsx"
                           id="image__cover--perview" style={{opacity: 0, display: 'none'}}/>
                    <label htmlFor="image__cover--perview">ارسال فایل اکسل</label>
                </div>
                <button
                    onClick={doMethFunction_2}
                    type="submit"
                    className="btn btn-dark form-group-chart text-size-medium btn-lg">
                    اجرای الگوریتم دوم
                    {/*<Link to={'/alg-two'}>اجرای الگوریتم دوم </Link>*/}
                </button>
            </div>
            <div className="form-group tree__group chart_form">
                <button
                    className="btn btn-dark form-group-chart text-size-medium btn-lg text-size">
                    <Link to={'/alg-one'}>نمایش اجرای الگوریتم اول </Link>
                </button>
                <button
                    className="btn btn-dark form-group-chart text-size-medium btn-lg text-size">
                    <Link to={'/alg-both'}>نمایش اجرای هردو الگوریتم </Link>
                </button>
                <button
                    className="btn btn-dark form-group-chart text-size-medium btn-lg">
                    <Link to={'/alg-two'}>نمایش اجرای الگوریتم دوم </Link>
                </button>
            </div>
        </div>
    );
}

export default Chart;