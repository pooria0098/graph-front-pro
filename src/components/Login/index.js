import React, {useContext} from "react";
import useForm from "../../hooks/useForm";
import axios from "axios";
import {API_URL} from "../../constants";
import {Link} from "react-router-dom";
import AuthContext from "../../context/AuthContext";
import {useHistory} from "react-router-dom";

const Login = () => {
    const [userName, setUserName, resetUserName] = useForm('')
    const [password, setPassword, resetPassword] = useForm('')
    const ctx = useContext(AuthContext);
    const history = useHistory()

    const loginUser = (event) => {
        event.preventDefault()
        axios.post(`${API_URL}/auth/login/`, {
            username: userName,
            password: password,
        }).then(res => {
            console.log('res.data login: ', res.data)
            ctx.onLogin(res.data.key)
            history.push('/')
        }).catch(err => {
            alert('Please SignUp First ')
            console.log('err: ', err)
        })
        resetUserName()
        resetPassword()
    }

    return (
        <form onSubmit={loginUser}>
            <h3 className="text-size-big">ورود</h3>

            <div className="form-group form-group_spec">
                <label className="text-size-medium" style={{textAlign:'right'}}>نام کاربری</label>
                <input
                    type="text"
                    value={userName}
                    style={{textAlign: 'right'}}
                    onChange={setUserName}
                    className="form-control text-size-small"
                    placeholder="نام کاربری را وارد کنید"/>
            </div>

            <div className="form-group form-group_spec">
                <label className="text-size-medium" style={{textAlign: 'right'}}>رمز</label>
                <input
                    type="password"
                    value={password}
                    style={{textAlign: 'right'}}
                    onChange={setPassword}
                    className="form-control text-size-small"
                    placeholder="رمز را وارد کنید"/>
            </div>

            {/*<div className="form-group">*/}
            {/*    <div className="custom-control custom-checkbox">*/}
            {/*        <input type="checkbox"*/}
            {/*               className="custom-control-input"*/}
            {/*               id="customCheck1"/>*/}
            {/*        <label className="custom-control-label"*/}
            {/*               htmlFor="customCheck1">*/}
            {/*            Remember me*/}
            {/*        </label>*/}
            {/*    </div>*/}
            {/*</div>*/}

            <button type="submit" style={{float: 'right'}}
                    className="btn btn-dark btn-lg btn-block text-size-small">
                ورود
            </button>
            <div className="clearfix"/>
            <p className="forgot-password text-right text-size-medium">
                قبلا ثبت نام نکرده اید؟ <Link to={"/sign-up"} className="text-size-medium text-black">ثبت نام</Link>
            </p>

        </form>
    )
}


export default Login