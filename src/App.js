import React, {useContext} from 'react';
import {
    Switch,
    Route,
    Link
} from "react-router-dom";
import AuthContext from "./context/AuthContext";
import Login from "./components/Login";
import SignUp from "./components/Singup";
import Home from "./components/Home";
import Chart from "./components/Chart";
import Charts from "./components/Chart/Charts";
import AlgOne from "./components/Chart/AlgOne";
import AlgTwo from "./components/Chart/AlgTwo";
import AlgBoth from "./components/Chart/AlgBoth";

const App = () => {
    const ctx = useContext(AuthContext)

    const onLogOut = () => {
        ctx.onLogout()
        localStorage.removeItem('treeId')
    }

    return (
        <div className="App">
            <nav className="nav bg-dark" id="navbar">
                <p className="nav-logo">
                    <span className="text-primary">
                        <i className="fas fa-book-open"/>درخت&nbsp;
                    </span>گراف
                </p>
                <ul className="nav-menu">
                    {!ctx.isLoggedIn &&
                    <li>
                        <Link to={"/sign-in"}>ورود</Link>
                    </li>}
                    {ctx.isLoggedIn &&
                    <>
                        <li>
                            <Link to={"/menu"}>جدول</Link>
                        </li>
                        <li>
                            <Link to={"/"}>درخت</Link>
                        </li>
                        <li>
                            <Link onClick={onLogOut} to={"/"}>خروج</Link>
                        </li>
                    </>}
                    {/*<li>*/}
                    {/*    <Link to={"/sign-up"}>Sign up</Link>*/}
                    {/*</li>*/}
                </ul>
            </nav>

            <div className="outer">
                <div className="inner">
                    <Switch>
                        {ctx.isLoggedIn && <>
                            <Route exact path='/' component={Home}/>
                            <Route exact path='/menu' component={Chart}/>
                            <Route exact path='/charts' component={Charts}/>
                            <Route exact path='/alg-one' component={AlgOne}/>
                            <Route exact path='/alg-two' component={AlgTwo}/>
                            <Route exact path='/alg-both' component={AlgBoth}/>
                        </>
                        }
                        <div className="inner_in">
                            <Route path="/sign-in" component={Login}/>
                            <Route path="/sign-up" component={SignUp}/>
                        </div>
                    </Switch>
                </div>
            </div>

        </div>
    )
}

export default App;
