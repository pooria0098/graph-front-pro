import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import {AuthContextProvider} from "./context/AuthContext";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'semantic-ui-css/semantic.min.css';
import './styles/entry.scss';
import App from './App';

ReactDOM.render(
    <AuthContextProvider>
        <Router>
            <App/>
        </Router>
    </AuthContextProvider>,
    document.getElementById('root')
);
serviceWorker.unregister();


// https://reactjsexample.com/a-simple-hierarchy-tree-view-for-react/
// https://daniel-hauser.github.io/react-organizational-chart/?path=/docs/example-tree--styled-nodes
// https://levelup.gitconnected.com/react-tree-graph-dcf96f8d5103
// https://codesandbox.io/s/rd3t-v2-custom-event-handlers-5pwxw?file=/src/index.js
// https://codepen.io/cloderic/pen/GygQVa?editors=1000
// https://codepen.io/zhulinpinyu/pen/EaZrmM
// https://codepen.io/eruixma/pen/WxLdAw
// https://react-spectrum.adobe.com/react-stately/useTreeData.html
// https://www.npmjs.com/package/react-d3-tree
// https://itnext.io/data-structures-in-js-binary-trees-react-app-5443b951a46b
