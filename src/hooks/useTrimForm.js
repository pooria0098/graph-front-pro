import {useState} from 'react';

const useTrimForm = initialVal => {
    const [state, setState] = useState(initialVal)

    const handleChange = (event, data) => {
        let targetData = ''
        if (event.target.value) {
            console.log('event.target.value: ',event.target.value)
            console.log('event.target.value.trim(): ',event.target.value.trim())
            targetData = event.target.value.trim()
        } else if (data) {
            targetData = data.value.trim()
        } else {
            targetData = ''
        }
        console.log('targetData: ', targetData)
        setState(targetData)
    }
    const reset = () => {
        setState(initialVal)
    }
    return [state, handleChange, reset]
}

export default useTrimForm;