import {useState} from 'react';

const useFormTwo = initialVal => {
    const [state, setState] = useState(initialVal)

    const handleChange = (event, data) => {
        let targetData = ''
        if (event.target.value) {
            targetData = event.target.value
            localStorage.setItem('treeId', targetData.toString())
            localStorage.setItem('treeId','2')
        } else if (data) {
            targetData = data.value
        } else {
            targetData = ''
        }
        console.log('targetData: ', targetData)
        setState(targetData)
    }
    const reset = () => {
        setState(initialVal)
    }
    return [state, handleChange, reset]
}

export default useFormTwo;