import React, {useState} from "react";

const AuthContext = React.createContext({
    isLoggedIn: undefined,
    onLogout: () => {
    },
    onLogin: () => {
    }
})

export const AuthContextProvider = (props) => {
    const userInfoStorage = localStorage.getItem('isLoggedIn')
    const isUserLoggedIn = !!userInfoStorage
    const [isLoggedIn, setIsLoggedIn] = useState(isUserLoggedIn);

    const loginHandler = (key) => {
        if (key) {
            localStorage.setItem('isLoggedIn', key)
            setIsLoggedIn(true);
        }
    };

    const logoutHandler = () => {
        localStorage.removeItem('isLoggedIn')
        setIsLoggedIn(false);
    };

    return (
        <AuthContext.Provider
            value={{
                isLoggedIn: isLoggedIn,
                onLogout: logoutHandler,
                onLogin: loginHandler
            }}>
            {props.children}
        </AuthContext.Provider>
    )
}

export default AuthContext