import React, {useEffect, useRef, useState} from 'react';
import {Network} from "vis-network";
import {API_URL} from "../../constants";
import axios from "axios";

const vi_network_style = {
    position: 'relative',
    overflow: 'hidden',
    touchAction: 'pan-y',
    userSelect: 'none',
    webkitUserDrag: 'none',
    webkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
    width: '100%',
    height: '100%'
}

const canvas_style = {
    position: 'relative',
    touchAction: 'none',
    userSelect: 'none',
    webkitUserDrag: 'none',
    webkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
    width: '100px',
    height: '100px',
}

const NodePosition = {
    0: {x: 150, y: -600},
    1: {x: 150, y: -450},
    2: {x: 150, y: -300},
    3: {x: 150, y: -150},
    4: {x: 150, y: 0},
    5: {x: 150, y: 150},
    6: {x: 150, y: 300},
    7: {x: 150, y: 450},
    8: {x: 150, y: 600},
    9: {x: 150, y: 750},
    10: {x: 150, y: 900},
}

// const nodes = [
//             {
//                 // widthConstraint: {minimum: 150},
//                 // widthConstraint: {maximum: 150},
//                 // heightConstraint: {minimum: 70},
//                 // heightConstraint: {maximum: 70},
//                 // heightConstraint: {minimum: 100, valign: "top"},
//                 // heightConstraint: {minimum: 100, valign: "middle"},
//                 // heightConstraint: {minimum: 100, valign: "bottom"},
//                 id: 100,
//                 label: "This node has no fixed, minimum or maximum width or height",
//                 widthConstraint: 150,
//                 x: -150,
//                 y: -600,
//             },
//             {
//                 id: 210,
//                 widthConstraint: 150,
//                 label: "This node has a mimimum width",
//                 x: -500,
//                 y: -450,
//             },
//             {
//                 id: 211,
//                 widthConstraint: 150,
//                 label: "...so does this",
//                 x: -600,
//                 y: -300,
//             },
//             {
//                 id: 220,
//                 widthConstraint: 150,
//                 label:
//                     "This node has a maximum width and breaks have been automatically inserted into the label",
//                 x: -200,
//                 y: -450,
//             },
//             {
//                 id: 221,
//                 widthConstraint: 150,
//                 label: "...this too",
//                 x: -100,
//                 y: -300,
//             },
//             {
//                 id: 200,
//                 font: {multi: true},
//                 widthConstraint: 150,
//                 label:
//                     "<b>This</b> node has a fixed width and breaks have been automatically inserted into the label",
//                 x: -350,
//                 y: -300,
//             },
//             {
//                 id: 201,
//                 widthConstraint: 150,
//                 label: "...this as well",
//                 x: -300,
//                 y: -150,
//             },
//             {
//                 id: 300,
//                 widthConstraint: 150,
//                 label: "This node\nhas a\nminimum\nheight",
//                 x: 100,
//                 y: -450,
//             },
//             {
//                 id: 301,
//                 widthConstraint: 150,
//                 label: "...this one here too",
//                 x: 100,
//                 y: -300,
//             },
//             {
//                 id: 400,
//                 widthConstraint: 150,
//                 label: "Minimum height\nvertical alignment\nmay be",
//                 x: 300,
//                 y: -450,
//             },
//             {
//                 id: 401,
//                 widthConstraint: 150,
//                 label: "Minimum height\nvertical alignment\nmay be middle\n(default)",
//                 x: 300,
//                 y: -300,
//             },
//             {
//                 id: 402,
//                 widthConstraint: 150,
//                 label: "Minimum height\nvertical alignment\nmay be bottom",
//                 x: 300,
//                 y: -150,
//             },
//         ];

// const edges = [
//             {from: 100, to: 210},
//             {from: 210, to: 211},
//             {from: 100, to: 220},
//             {from: 220, to: 221},
//             {from: 210, to: 200},
//             {from: 220, to: 200},
//             {from: 200, to: 201},
//             {from: 100, to: 300},
//             {from: 300, to: 301},
//             {from: 100, to: 400},
//             {from: 400, to: 401},
//             {from: 401, to: 402},
//             // {from: 401, to: 402,
//             //     widthConstraint: {maximum: 150},
//             //     label: "middle valign to bottom valign",
//             // },
//         ];

function Tree({treeId, Nodes, propertiesChanged}) {
    const [nodes, setNodes] = useState([])
    const [edges, setEdges] = useState([])
    const containerRef = useRef();

    console.log('TREE')
    console.log('Nodes: ', Nodes)

    useEffect(() => {
        let temp = []
        let temp1 = []

        async function fetchData() {
            const response = await axios.get(`${API_URL}/node/list/${treeId}/`)
            console.log('res.data TREE: ', response.data)

            // Edges
            const filteredNodes = response.data.filter(node => node.reff.length > 0)
            console.log('filteredNodes1: ', filteredNodes)
            console.log('filteredNodes: ', filteredNodes.map(node => {
                return node.reff.map((item, ide) => {
                    return {
                        from: parseInt(node.id), to:
                            parseInt(item.node_ref)
                    }
                })

            }))
            temp1 = filteredNodes.map(node => {
                    return node.reff.map((item, ide) => {
                        return {
                            from: parseInt(node.id), to:
                                parseInt(item.node_ref)
                        }
                    })

                }
            )
            await setEdges(
                filteredNodes.map(node => {
                        return node.reff.map((item, ide) => {
                            return {
                                from: parseInt(node.id), to:
                                    parseInt(item.node_ref)
                            }
                        })

                    }
                )
            )

            // Nodes
            // const uniqueResponse = Array.from(new Set(response.data.map(node => node.node_name))).map(node_name => {
            //     return response.data.find(node => node.node_name === node_name)
            // })
            // console.log('uniqueResponse: ', uniqueResponse)
            temp = response.data.map(node => (
                {
                    id: node.id,
                    font: {multi: true},
                    widthConstraint: 150,
                    // eslint-disable-next-line no-useless-concat
                    label: "(<b>" + `${node.node_name}` + "</b>) => " + node.properties.map(node => node.property_name).join(' - '),
                    // x: NodePosition[node.row_name_number].x,
                    x: node.node_horizontal_position,
                    y: NodePosition[node.row_name_number].y,
                }
            ))
            await setNodes(
                response.data.map(node => (
                    {
                        id: node.id,
                        font: {multi: true},
                        widthConstraint: 150,
                        // eslint-disable-next-line no-useless-concat
                        label: "(<b>" + `${node.node_name}` + "</b>) => " + node.properties.map(node => node.property_name).join(' - '),
                        // x: NodePosition[node.row_name_number].x,
                        x: node.node_horizontal_position,
                        y: NodePosition[node.row_name_number].y,
                    }
                ))
            )
            console.log('nodes inside: ', response.data.map(node => (
                {
                    id: node.id,
                    font: {multi: true},
                    widthConstraint: 150,
                    // eslint-disable-next-line no-useless-concat
                    label: "(<b>" + `${node.node_name}` + "</b>) => " + node.properties.map(node => node.property_name).join(' - '),
                    // x: NodePosition[node.row_name_number].x,
                    x: node.node_horizontal_position,
                    y: NodePosition[node.row_name_number].y,
                }
            )))


            console.log('nodes outside: ', temp)
            console.log('edges: ', temp1)
            const data = {
                nodes: temp,
                edges: temp1,
            };
            const options = {
                edges: {
                    font: {
                        size: 12,
                    },
                    widthConstraint: {
                        maximum: 90,
                    },
                },
                nodes: {
                    shape: "box",
                    margin: 10,
                    widthConstraint: {
                        maximum: 200,
                    },
                },
                physics: {
                    enabled: false,
                },
            };
            console.log('containerRef.current: ', containerRef.current)
            new Network(containerRef.current, data, options);
        }

        fetchData()


    }, [Nodes, nodes.length, edges.length, propertiesChanged])


    return (
        <>
            {console.log('NODES IN RENDER: ', nodes)}
            <div ref={containerRef} id="mynetwork">
                <div className="vis-network" tabIndex="0" style={vi_network_style}>
                    <canvas width="750" style={canvas_style} height="500"/>
                </div>
            </div>
        </>
    )
}

export default Tree;